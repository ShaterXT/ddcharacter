#ifndef STATS_H
#define STATS_H
#include <iostream>


class Stats
{
    public:
        Stats();
        void add(int, int, int, int, int, int);
        void showMod();
        virtual ~Stats();

    protected:
        int modSila;
        int modZrecznosc;
        int modBudowa;
        int modInteligencja;
        int modMadrosc;
        int modCharyzma;
    private:
        int sila;
        int zrecznosc;
        int budowa;
        int inteligencja;
        int madrosc;
        int charyzma;

        int modifire();
};

#endif // STATS_H
