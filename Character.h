#include <iostream>
#ifndef CHARACTER_H
#define CHARACTER_H
#include "Stats.h"

using namespace std;

class Character:private Stats
{
    private:
    string name;
    string player;
    int years;

    protected:

    public:
        Character();
        void dodaj();
        void show();
        virtual ~Character();
};



#endif // CHARACTER_H
