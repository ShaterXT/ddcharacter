#include "Stats.h"
#include <iostream>
using namespace std;

void Stats::add(int si=8,int zr=8,int bd=8,int it=8,int md=8,int ch=8)
{
    sila=si;
    zrecznosc=zr;
    budowa=bd;
    inteligencja=it;
    madrosc=md;
    charyzma=ch;
    modifire();
};

Stats::modifire()
{
    modSila=(sila-10)/2;
    modZrecznosc=(zrecznosc-10)/2;
    modBudowa=(budowa-10)/2;
    modInteligencja=(inteligencja-10)/2;
    modMadrosc=(madrosc-10)/2;
    modCharyzma=(charyzma-10)/2;
};

void Stats::showMod()
{
    cout << "Sila: " << sila<<endl;
    cout << "zrecznosc: " << zrecznosc<<endl;
    cout << "budowa: " << budowa<<endl;
    cout << "inteligencja: " << inteligencja<<endl;
    cout << "madrosc: " << madrosc<<endl;
    cout << "charyzma: " << charyzma<<endl;
    cout << "Mody:"<<endl;
    cout << "modSila: " << modSila<<endl;
    cout << "modZrecznosc: " << modZrecznosc<<endl;
    cout << "modBudowa: " << modBudowa<<endl;
    cout << "modInteligencja: " << modInteligencja<<endl;
    cout << "modMadrosc: " << modMadrosc<<endl;
    cout << "modCharyzma: " << modCharyzma<<endl;
}

Stats::~Stats()
{
    //dtor
}
